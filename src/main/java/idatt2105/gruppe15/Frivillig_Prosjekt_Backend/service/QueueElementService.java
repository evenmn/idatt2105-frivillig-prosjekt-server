package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.service;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.QueueElement;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.repository.QueueElementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

@Service
public class QueueElementService {

    @Autowired
    QueueElementRepository queueElementRepository;

    /**
     * Calls for repo to retrieve queue elements from database and adds a list of students corresponding to this queue element
     * @param email
     * @param courseId
     * @return
     */
    public List<QueueElement> getAllQueues(String email, Long courseId) {
        List<QueueElement> qe = new ArrayList<>();
        if (email == null && courseId == null) {
            queueElementRepository.findAll().forEach(qe::add);
        } else if (email == null){
            queueElementRepository.findByCourseId(courseId).forEach(qe::add);
        } else if (courseId == null){
            queueElementRepository.findByStudentEmail(email).forEach(qe::add);
        } else {
            queueElementRepository.findByCourseIdAndEmail(courseId, email).forEach(qe::add);
        }

        for (int i = 0; i < qe.size(); i++) {
            setObligList(qe.get(i));
            qe.get(i).setUserEmail(queueElementRepository.findStudents(qe.get(i).getQueueId()));
        }

        return qe;
    }

    /**
     * Calls for queue elements from repo and adds corresponding students
     * @param id
     * @return
     */
    public QueueElement getQueueById(Long id) {
        QueueElement qe = queueElementRepository.findById(id);
        setObligList(qe);
        qe.setUserEmail(queueElementRepository.findStudents(qe.getQueueId()));
        return qe;
    }

    /**
     * Calls repo to add queue element to database and then add list of students
     * @param qe
     */
    public void createQueueElement(QueueElement qe) {
        setObligString(qe);
        queueElementRepository.save(qe);
        QueueElement dbqe = queueElementRepository.findWithoutId(qe);
        qe.setQueueId(dbqe.getQueueId());
        for (int i = 0; i < qe.getUserEmail().size(); i++) {
            queueElementRepository.assignStudent(qe.getQueueId(), qe.getUserEmail().get(i));
        }
    }

    public boolean updateQueueElement(Long id, QueueElement qe) {
        setObligString(qe);
        QueueElement _qe = queueElementRepository.findById(id);
        if (_qe != null) {
            _qe.setCourseId(id);
            _qe.setObligNr(qe.getObligNr());
            _qe.setDate(qe.getDate());
            _qe.setCampus(qe.getCampus());
            _qe.setBuilding(qe.getBuilding());
            _qe.setRoom(qe.getRoom());
            _qe.setSeat(qe.getSeat());
            _qe.setValidation(qe.getValidation());
            _qe.setMessage(qe.getMessage());
            _qe.setUserEmail(qe.getUserEmail());
            _qe.setCourseId(qe.getCourseId());
            queueElementRepository.update(_qe);
            return true;
        } else {
            return false;
        }
    }

    public int deleteQueueElement(Long id) {
        return queueElementRepository.deleteById(id);
    }

    public int deleteAllQueues(Long courseId) {
        if (courseId == null) {
            return queueElementRepository.deleteAll();
        }
        return queueElementRepository.clearQueue(courseId);
    }

    public void assignAssistant(Long id, String email) {
        queueElementRepository.assignAssistant(id, email);
    }

    public int removeAssistant(Long id) {
        return queueElementRepository.removeAssistant(id);
    }

    /**
     * Takes list of obligs in the queue element object and translates them into a string to be stored in database
     * @param qe
     */
    public void setObligList(QueueElement qe) {
        List<Integer> obligList = new ArrayList<>();
        StringTokenizer str = new StringTokenizer(qe.getObligNr(), "-");
        while (str.hasMoreTokens()) {
            obligList.add(Integer.parseInt(str.nextToken()));
        }
        qe.setObligList(obligList);
    }

    /**
     * Takes oblig string from database and translates into a list of numbers corresponding to the oblig number
     * @param qe
     */
    public void setObligString(QueueElement qe) {
        String obligString = "";
        List<Integer> obligList = qe.getObligList();
        for (int i = 0; i < obligList.size() - 1; i++) {
            obligString += obligList.get(i);
            obligString += "-";
        }
        obligString += obligList.get(obligList.size() - 1);
        qe.setObligNr(obligString);
    }
}
