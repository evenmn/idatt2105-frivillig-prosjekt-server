package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.service;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.Course;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.repository.CourseRepository;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.repository.QueueElementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CourseService {

    @Autowired
    CourseRepository courseRepository;

    @Autowired
    QueueElementRepository queueElementRepository;

    /**
     * Calls for courses from database, and adds potential assistant from separate table
     * @param email
     * @return
     */
    public List<Course> getAllCourses(String email) {
        List<Course> courses = new ArrayList<>();
        if (email == null) {
            courseRepository.findAll().forEach(courses::add);
        } else {
            courseRepository.findByStudentEmail(email).forEach(courses::add);
        }

        for (int i = 0; i < courses.size(); i++) {
            courses.get(i).setAssistants(courseRepository.findAssistantEmails(courses.get(i).getCourseId()));
        }

        return courses;
    }

    /**
     * Calls for courses assigned to specified student and retrieves assistants for the course in question
     * @param email
     * @return
     */
    public List<Course> getCourseByAssistant(String email) {
        List<Course> courses = new ArrayList<>();
        courseRepository.findByAssistantEmail(email).forEach(courses::add);

        for (int i = 0; i < courses.size(); i++) {
            courses.get(i).setAssistants(courseRepository.findAssistantEmails(courses.get(i).getCourseId()));
        }
        return courses;
    }

    /**
     * Calls for single course and retrieves assistants for course in question
     * @param id
     * @return
     */
    public Course getCourseById(Long id) {
        Course course = courseRepository.findById(id);
        if (course != null) {
            course.setAssistants(courseRepository.findAssistantEmails(id));
        }
        return course;
    }

    public Course getCourseWithoutId(Course course) {
        return courseRepository.findWithoutId(course);
    }

    public void createCourse(Course course) {
        courseRepository.save(course);
    }

    public boolean updateCourse(Long id, Course course) {
        Course _course = courseRepository.findById(id);
        if (_course != null) {
            _course.setCourseId(id);
            _course.setCourseCode(course.getCourseCode());
            _course.setCourseName(course.getCourseName());
            _course.setTotObligs(course.getTotObligs());
            _course.setRequiredObligs(course.getRequiredObligs());
            _course.setCourseInformation(course.getCourseInformation());
            _course.setCampus(course.getCampus());
            _course.setBuilding(course.getBuilding());
            _course.setRoom(course.getRoom());
            _course.setNumOfTables(course.getNumOfTables());
            _course.setYear(course.getYear());
            _course.setCourseActive(course.getCourseActive());
            _course.setActiveQueue(course.getActiveQueue());
            courseRepository.update(_course);
            return true;
        } else {
            return false;
        }
    }

    public int getQueueSize(Long id) {
        return courseRepository.getQueueSize(id);
    }

    public int deleteCourse(Long id) {
        queueElementRepository.clearQueue(id);
        return courseRepository.deleteById(id);
    }

    public int deleteAllCourses() {
        queueElementRepository.deleteAll();
        return courseRepository.deleteAll();
    }

    public void addAssistant(Long id, String email) {
        courseRepository.addAssistant(id, email);
    }

    public int removeAssistant(Long id, String email) {
        return courseRepository.removeAssistant(id, email);
    }

    public int removeAllAssistants(long courseId) {
        return courseRepository.removeAllAssistants(courseId);
    }
}
