package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.service;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.Approval;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.StudentCourse;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.repository.StudentCourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentCourseService {

    @Autowired
    StudentCourseRepository studentCourseRepository;

    /**
     * Calls repo to fetch student-course combinations and adds a list of their approved obligs
     * @return
     */
    public List<StudentCourse> getAllSc() {
        List<StudentCourse> sc = new ArrayList<>();
        studentCourseRepository.findAll().forEach(sc::add);

        for (int i = 0; i < sc.size(); i++) {
            sc.get(i).setObligs(studentCourseRepository.findObligs(sc.get(i)));
        }

        return sc;
    }

    public void assignStudent(StudentCourse sc) {
        studentCourseRepository.save(sc);
    }

    public int deleteStudentCourse(StudentCourse sc) {
        return studentCourseRepository.deleteByStudentCourse(sc);
    }

    public int deleteAllStudentCourses() {
        return studentCourseRepository.deleteAll();
    }

    public List<Integer> findObligs(StudentCourse sc) {
        return studentCourseRepository.findObligs(sc);
    }

    public void approveOblig(Approval approval) {
        studentCourseRepository.approveOblig(approval);
    }

    public int deleteAllApprovals() {
        return studentCourseRepository.deleteAllApprovals();
    }
}
