package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.service;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.Credentials;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.SeveralUsers;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.StudentCourse;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.User;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.repository.StudentCourseRepository;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.lang3.RandomStringUtils;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.StringTokenizer;

@Service
public class UserService {

    Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    UserRepository userRepository;

    @Autowired
    StudentCourseRepository studentCourseRepository;

    public List<User> getAllUsers(Long id) {
        List<User> users = new ArrayList<>();
        if (id == null) {
            userRepository.findAll().forEach(users::add);
        } else {
            userRepository.findStudentsByCourseId(id).forEach(users::add);
        }
        return users;
    }

    public List<User> getAllAssistants(Long id) {
        List<User> assistants = new ArrayList<User>();
        if (id == null) {
            userRepository.findAllAssistants().forEach(assistants::add);
        } else {
            userRepository.findAssistantsByCourseId(id).forEach(assistants::add);
        }
        return assistants;
    }

    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    /**
     * Hashes password and adds user to the database
     * @param user
     * @param password
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public void createUser(User user, String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
        password = hashPassword(user.getUserEmail(), password);
        userRepository.save(user, password);
    }

    public boolean updateUser(String email, User user) {
        User _user = userRepository.findByEmail(email);
        if (_user != null) {
            _user.setUserEmail(email);
            _user.setFirstName(user.getFirstName());
            _user.setLastName(user.getLastName());
            _user.setAdmin(user.getAdmin());
            userRepository.update(_user);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Hashes password and adds updates the hash already stored in the database
     * @param credentials
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public boolean updatePassword(Credentials credentials) throws NoSuchAlgorithmException, InvalidKeySpecException {
        Credentials _credentials = userRepository.findCredentialsByEmail(credentials.getUserEmail());
        if(_credentials != null) {
            credentials.setPassword(hashPassword(credentials.getUserEmail(), credentials.getPassword()));
            userRepository.updatePassword(credentials);
            return true;
        } else {
            return false;
        }
    }

    public int deleteUser(String email) {
        return userRepository.deleteByEmail(email);
    }

    public int deleteAllUsers() {
        return userRepository.deleteAll();
    }

    public List<User> findAdmins() {
        return userRepository.findAdmins();
    }

    public List<User> findStudents() {
        return userRepository.findStudents();
    }

    public boolean validatePassword(Credentials credentials) throws NoSuchAlgorithmException, InvalidKeySpecException {
        credentials.setPassword(hashPassword(credentials.getUserEmail(), credentials.getPassword()));
        return userRepository.validatePassword(credentials);
    }

    /**
     * Takes a string of user information and adds those users to the course in question.
     * If the user does not already exist in the database, a new user will be created before they are added
     * @param users A string on the format "email, firstName, lastName".
     *              A lineshift indicates a new user
     * @param courseId The unique course id of the course the users should be added into
     * @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException
     */
    public void createUsersFromString(SeveralUsers users, long courseId) throws InvalidKeySpecException, NoSuchAlgorithmException {
        String userString = users.getUserString();

        StringTokenizer lines = new StringTokenizer(userString, "\n");

        while (lines.hasMoreTokens()) {
            StringTokenizer userInfo = new StringTokenizer(lines.nextToken(), ", ");
            String email = userInfo.nextToken();
            String firstName = userInfo.nextToken();
            String lastName = userInfo.nextToken();

            logger.info("registering user: " + firstName + ", " + lastName + ", " + email + " for course: " + courseId);

            String password = RandomStringUtils.random(15, true, true);
            logger.info("Generated random password: " + password);

            //Send password via email to user

            String hashedPassword = hashPassword(email, password);

            User user = new User(email, firstName, lastName);
            StudentCourse sc = new StudentCourse(email, courseId);

            try {
                userRepository.save(user, hashedPassword);
            } catch (Exception e) {
                logger.info("Couldn't create user");
            }
            try {
                studentCourseRepository.save(sc);
            } catch (Exception e) {
                logger.info("couldn't add student to course");
            }
        }
    }

    /**
     * Hashes the password using th user's email to create a salt
      * @param email
     * @param password
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public String hashPassword(String email, String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
        PBEKeySpec pbeKeySpec = new PBEKeySpec(password.toCharArray(), email.getBytes(StandardCharsets.UTF_8), 10, 512);

        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
        byte[] hash = skf.generateSecret(pbeKeySpec).getEncoded();

        String base64Hash = Base64.getUrlEncoder().encodeToString(hash);
        logger.info("Hashed password to: " + base64Hash);
        return base64Hash;
    }
}
