package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.web;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.QueueElement;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.service.QueueElementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class QueueElementController {

    @Autowired
    QueueElementService queueElementService;

    Logger logger = LoggerFactory.getLogger(QueueElementController.class);

    /**
     * Returns a list of all queue elements within the database
     * @param email Optional parameter
     *              If included, the call will look for queue elements belonging to the specified user
     * @param courseId Optional parameter
     *                 If included, the call will look for queue elements belonging to the specified course
     * @return
     */
    @GetMapping("/queue")
    public ResponseEntity<List<QueueElement>> getAllQueues(@RequestParam(required = false) String email,
                                                           @RequestParam(required = false) Long courseId) {

        logger.info("Called getQueue with email='" + email + "' and courseId=" + courseId);
        try {
            List<QueueElement> qe = queueElementService.getAllQueues(email, courseId);
            if (qe.isEmpty()) {
                logger.info("no match for this queue element");
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            logger.info("Queue element(s) found!");
            return new ResponseEntity<>(qe, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Returns queue element object corresponding to the specified id
     * @param id
     * @return
     */
    @GetMapping("/queue/{id}")
    public ResponseEntity<QueueElement> getQueueById(@PathVariable("id") Long id) {
        QueueElement qe = queueElementService.getQueueById(id);
        if (qe != null) {
            return new ResponseEntity<>(qe, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds a new queue element to the database
     * @param qe
     * @return
     */
    @PostMapping("/queue")
    public ResponseEntity<String> createQueueElement(@RequestBody QueueElement qe) {
        try {
            queueElementService.createQueueElement(qe);
            return new ResponseEntity<>("Queue element was created successfully.", HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Updates a queue element existing in the database
     * @param id Id of the the queue element that should be updated
     * @param qe Queue element object containing updated information
     * @return
     */
    @PutMapping("/queue/{id}")
    public ResponseEntity<String> updateQueueElement(@PathVariable("id") Long id, @RequestBody QueueElement qe) {
        boolean response = queueElementService.updateQueueElement(id, qe);
        if (response) {
            return new ResponseEntity<>("Queue element was updated successfully.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Cannot find queue element with id=" + id, HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes a queue element from the database
     * @param id
     * @return
     */
    @DeleteMapping("/queue/{id}")
    public ResponseEntity<String> deleteQueueElement(@PathVariable("id") Long id) {
        try {
            int result = queueElementService.deleteQueueElement(id);
            if (result == 0) {
                return new ResponseEntity<>("Cannot find queue element with id=" + id, HttpStatus.OK);
            }
            return new ResponseEntity<>("Queue element was deleted successfully.", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("Cannot delete queue element.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Deletes ALL queue elements from the database
     * @param courseId
     * @return
     */
    @DeleteMapping("/queue")
    public ResponseEntity<String> deleteAllQueues(@RequestParam(required = false) Long courseId) {
        try {
            int numRows = queueElementService.deleteAllQueues(courseId);
            return new ResponseEntity<>("Deleted " + numRows + " queue element(s) successfully.", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("Cannot delete queue elements.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Assigns an assistant to evaluate the assignment in question
     * @param id
     * @param email
     * @return
     */
    @PostMapping("/queue/{id}/{email}")
    public ResponseEntity<String> assignAssistant(@PathVariable("id") Long id, @PathVariable("email") String email) {
        try {
            queueElementService.assignAssistant(id, email);
            return new ResponseEntity<>("Assistant was assigned successfully.", HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Removes assistant currently evaluating assignment
     * @param id
     * @return
     */
    @DeleteMapping("/queue/{id}/assistants")
    public ResponseEntity<String> removeAssistant(@PathVariable("id") Long id) {
        try {
            int result = queueElementService.removeAssistant(id);
            if (result == 0) {
                return new ResponseEntity<>("Cannot find queue element with id=" + id, HttpStatus.OK);
            }
            return new ResponseEntity<>("Assistant was removed successfully.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Cannot remove assistant.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
