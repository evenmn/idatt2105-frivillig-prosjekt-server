package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.web;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.Credentials;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.User;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.service.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.web.bind.annotation.*;

import java.security.Key;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/token")
@EnableAutoConfiguration
@CrossOrigin
public class TokenController {

    public static String keyStr = "TheIndustrialRevolutionanditsconsequenceshavebeenadisasterforthehumanrace";

    @Autowired
    UserService userService;

    Logger logger = LoggerFactory.getLogger(TokenController.class);

    /**
     * Validates user and returns a token to be stored on the client side
     * @param credentials
     * @return
     */
    @PostMapping("")
    public String generateToken(@RequestBody Credentials credentials) {
        logger.info("Received generate token call for user: " + credentials.getUserEmail());

        try {
            final String email = credentials.getUserEmail();

            boolean response = userService.validatePassword(credentials);

            if (response) {
                String token = generateToken(email);
                logger.info("Successfully generated token");
                return token;
            }
            return "Access denied";
        } catch (Exception e) {
            e.printStackTrace();
            return "Access denied";
        }
    }

    /**
     * Generates unique token for the user
     * @param userId In our case, the user's email address
     * @return
     * @throws Exception
     */
    public String generateToken(String userId) throws Exception {
        Key key = Keys.hmacShaKeyFor(keyStr.getBytes("UTF-8"));
        List<GrantedAuthority> grantedAuthorities = AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER");

        Claims claims = Jwts.claims().setSubject(userId);
        claims.put("userId", userId);
        claims.put("authorities", grantedAuthorities
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList()));

        return Jwts.builder()
                .setId(UUID.randomUUID().toString())
                .setSubject(userId)
                .setClaims(claims)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + 600000000))
                .signWith(key)
                .compact();
    }
}
