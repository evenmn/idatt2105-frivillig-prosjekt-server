package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.web;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.Course;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.service.CourseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class CourseController {

    @Autowired
    CourseService courseService;

    Logger logger = LoggerFactory.getLogger(CourseController.class);

    /**
     * Returns a list of all courses
     * @param email Optional paramater
     *              If included, the call will return all courses that a student
     *              with the specified email address is assigned to
     * @return
     */
    @GetMapping("/courses")
    public ResponseEntity<List<Course>> getAllCourses(@RequestParam(required = false) String email) {

        logger.info("Received request for courses from user: " + email);
        try {
            List<Course> courses = courseService.getAllCourses(email);
            if (courses.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            for (int i = 0; i < courses.size(); i++) {
                logger.info("retreived course: " + courses.get(i).getCourseCode());
            }
            return new ResponseEntity<>(courses, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Returns a list of courses to which a specified student is assigned as asistant
     * @param email
     * @return
     */
    @GetMapping("/courses/assistant/{email}")
    public ResponseEntity<List<Course>> getCourseByAssistant(@PathVariable("email") String email) {

        logger.info("Received request for assistant courses from user: " + email);
        try {
            List<Course> courses = courseService.getCourseByAssistant(email);
            if (courses.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            for (int i = 0; i < courses.size(); i++) {
                logger.info("retreived course: " + courses.get(i).getCourseCode());
            }
            return new ResponseEntity<>(courses, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Returns a single course corresponding to the specified course id
     * @param id
     * @return
     */
    @GetMapping("/courses/{id}")
    public ResponseEntity<Course> getCourseById(@PathVariable("id") Long id) {
        Course course = courseService.getCourseById(id);
        if (course != null) {
            return new ResponseEntity<>(course, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Since courseId is auto generated, there may be times where we need to obtain a course without knowing the id
     * This call will reference the database to find a course with matching course code AND year as the coure object provided
     * @param course
     * @return
     */
    @PostMapping("/coursesNoId")
    public ResponseEntity<Course> getCourseWithoutId(@RequestBody Course course) {
        Course newCourse = courseService.getCourseWithoutId(course);
        if (course != null) {
            return new ResponseEntity<>(newCourse, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Returns the size of the queue of a specified course
     * @param id
     * @return
     */
    @GetMapping("/courses/queue/{id}")
    public ResponseEntity<Integer> getQueueSize(@PathVariable("id") Long id) {
        Integer size = courseService.getQueueSize(id);
        if (size != null) {
            return new ResponseEntity<>(size, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds a new course to the database
     * @param course
     * @return
     */
    @PostMapping("/courses")
    public ResponseEntity<String> createCourse(@RequestBody Course course) {
        try {
            courseService.createCourse(course);
            return new ResponseEntity<>("Course was created successfully.", HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Updates an existing course within the database
     * @param id Id of the course to be updated
     * @param course Course object containing new course information
     * @return
     */
    @PutMapping("/courses/{id}")
    public ResponseEntity<String> updateCourse(@PathVariable("id") Long id, @RequestBody Course course) {

        logger.info("called update course on courseId=" + id);
        boolean response = courseService.updateCourse(id, course);
        if (response) {
            logger.info("Course updated successfully");
            return new ResponseEntity<>("Course was updated successfully.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Cannot find course with id=" + id, HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Deletes specified course from database.
     * This will also delete any correlation to other tables
     * @param id
     * @return
     */
    @DeleteMapping("/courses/{id}")
    public ResponseEntity<String> deleteCourse(@PathVariable("id") Long id) {
        try {
            int result = courseService.deleteCourse(id);
            if (result == 0) {
                return new ResponseEntity<>("Cannot find course with id=" + id, HttpStatus.OK);
            }
            return new ResponseEntity<>("Course was deleted successfully.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Cannot delete course.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Deletes all courses within the database
     * This will also delete all corresponding values in other tables,
     * meaning that in practice all tables besides USERS will be wiped clean
     * @return
     */
    @DeleteMapping("/courses")
    public ResponseEntity<String> deleteAllCourses() {
        try {
            int numRows = courseService.deleteAllCourses();
            return new ResponseEntity<>("Deleted " + numRows + " course(s) successfully.", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("Cannot delete courses.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Assigns a student as an assistant to a specified course
     * @param id
     * @param email
     * @return
     */
    @PostMapping("/courses/{id}/{email}")
    public ResponseEntity<String> addAssistant(@PathVariable("id") Long id, @PathVariable("email") String email) {
        logger.info("Called add asiistant for courseId=" + id + ", assistant email: " + email);
        try {
            courseService.addAssistant(id, email);
            return new ResponseEntity<>("Assistant was added successfully.", HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Removes a specified assistant from course
     * @param id
     * @param email
     * @return
     */
    @DeleteMapping("/courses/assistant/{id}/{email}")
    public ResponseEntity<String> removeAssistant(@PathVariable("id") Long id, @PathVariable("email") String email) {
        try {
            int result = courseService.removeAssistant(id, email);
            if (result == 0) {
                return new ResponseEntity<>("Cannot find assistant with email=" + email + " and course id=" + id, HttpStatus.OK);
            }
            return new ResponseEntity<>("Assistant was removed successfully.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Cannot remove assistant.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Removes ALL assistants from course
     * @param id
     * @return
     */
    @DeleteMapping("/courses/assistant/{id}")
    public ResponseEntity<String> removeAllAssistants(@PathVariable("id") Long id) {
        logger.info("called removed all assistants for courseId=" + id);
        try {
            int result = courseService.removeAllAssistants(id);
            if (result == 0) {
                return new ResponseEntity<>("Cannot find assistant with course id=" + id, HttpStatus.OK);
            }
            return new ResponseEntity<>("Assistant(s) were removed successfully.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Cannot remove assistants.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
