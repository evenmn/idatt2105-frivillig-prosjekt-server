package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.web;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.Approval;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.StudentCourse;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.service.StudentCourseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class StudentCourseController {

    @Autowired
    StudentCourseService studentCourseService;

    Logger logger = LoggerFactory.getLogger(StudentCourseController.class);

    /**
     * Returns list of all student-course combinations
     * @return
     */
    @GetMapping("/sc")
    public ResponseEntity<List<StudentCourse>> getAllStudentCourses() {
        try {
            List<StudentCourse> sc = studentCourseService.getAllSc();
            if (sc.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(sc, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Assigns a student to a course
     * @param sc Object containing student email and course id
     * @return
     */
    @PostMapping("/sc")
    public ResponseEntity<String> assignStudent(@RequestBody StudentCourse sc) {
        try {
            studentCourseService.assignStudent(sc);
            return new ResponseEntity<>("Student was assigned successfully.", HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Removes student from course
     * @param sc Object containing student email and course id
     * @return
     */
    @DeleteMapping("/sc")
    public ResponseEntity<String> deleteStudentFromCourse(@RequestBody StudentCourse sc) {
        try {
            if (sc != null) {
                int result = studentCourseService.deleteStudentCourse(sc);
                if (result == 0) {
                    return new ResponseEntity<>("Cannot find student with email=" + sc.getUserEmail() +
                            " and courseId=" + sc.getCourseId(), HttpStatus.OK);
                }
                return new ResponseEntity<>("Student was removed successfully.", HttpStatus.OK);
            } else {
                int numRows = studentCourseService.deleteAllStudentCourses();
                return new ResponseEntity<>("Deleted " + numRows + " user(s) successfully.", HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>("Cannot delete user(s).", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Returnns a list of all obligs that have been approved for specified student-course combination
     * @param sc Object containing student email and course id
     * @return
     */
    @PostMapping("/sc/obliglist")
    public ResponseEntity<List<Integer>> findObligs(@RequestBody StudentCourse sc) {
        logger.info("called find obligs for user: " + sc.getUserEmail());
        try {
            List<Integer> obligs = studentCourseService.findObligs(sc);
            return new ResponseEntity<>(obligs, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Marks one oblig as approved
     * @param approval Object containing student email, course id, oblig number and an optinal feedback message
     * @return
     */
    @PostMapping("/sc/oblig")
    public ResponseEntity<String> approveOblig(@RequestBody Approval approval) {

        logger.info("called approve oblig for user: " + approval.getUserEmail() + ", courseId: " + approval.getCourseId() + ", obligNr: " + approval.getObligNr());
        try {
            studentCourseService.approveOblig(approval);
            return new ResponseEntity<>("Oblig was approved successfully.", HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Marks all obligs as 'not approved'
     * Mostly used for testing
     * @return
     */
    @DeleteMapping("/sc/oblig")
    public ResponseEntity<String> deleteAllApprovals() {
        try {
            int numRows = studentCourseService.deleteAllApprovals();
            return new ResponseEntity<>("Deleted " + numRows + " oblig(s) successfully.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("Cannot delete oblig(s).", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
