package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.web;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.Credentials;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.SeveralUsers;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.User;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    UserService userService;

    Logger logger = LoggerFactory.getLogger(UserController.class);

    /**
     * Returns a list of all users registered in the database
     * @param courseId Optional parameter.
     *                 If included, the call returns a list of all users registered in a specified course
     * @return
     */
    @GetMapping("/users")
    public ResponseEntity<List<User>> getAllUsers(@RequestParam(required = false) Long courseId) {
        try {
            List<User> users = userService.getAllUsers(courseId);
            if (users.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(users, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Validates if a email-password combination corresponds to a user in the database
     * @param credentials
     * @return
     */
    @PostMapping("/validation")
    public ResponseEntity<Boolean> validateUser(@RequestBody Credentials credentials) {
        logger.info("received validation call from " + credentials.getUserEmail());
        try {
            boolean response = userService.validatePassword(credentials);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * Returns all a list of students that are registered as assistants
     * @param courseId Optional parameter.
     *                 If included, the call will return all assistants assigned to a certain course
     * @return
     */
    @GetMapping("/users/assistants")
    public ResponseEntity<List<User>> getAllAssistants(@RequestParam(required = false) Long courseId) {
        try {
            List<User> assistants = userService.getAllAssistants(courseId);
            if (assistants.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(assistants, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Returns a single user object connected to specified email address
      * @param email
     * @return
     */
    @GetMapping("/users/{email}")
    public ResponseEntity<User> getUserByEmail(@PathVariable("email") String email) {
        logger.info("Received getUserByEmail call for: " + email);
        User user = userService.getUserByEmail(email);
        if (user != null) {
            logger.info("Retrieve user Success");
            return new ResponseEntity<>(user, HttpStatus.OK);
        } else {
            logger.info("User not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Adds a new user object to the database
     * @param user
     * @param password
     * @return
     */
    @PostMapping("/users/{password}")
    public ResponseEntity<String> createUser(@RequestBody User user, @PathVariable("password") String password) {
        try {
            userService.createUser(user, password);
            return new ResponseEntity<>("User was created successfully.", HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Registers several users from a string on the format 'email, firstName, lastName\n' to the specified course
     * If the user isn't already registered in the database, they will be added with an auto generated password
     * @param users
     * @param courseId
     * @return
     */
    @PostMapping("/users/register/{courseId}")
    public ResponseEntity<String> createUsersFromString(@RequestBody SeveralUsers users, @PathVariable("courseId") long courseId) {
        logger.info("called several users");
        try {
            userService.createUsersFromString(users, courseId);
            return new ResponseEntity<>("User was created successfully.", HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Updates a user object
     * @param email email of user to be updated
     * @param user user object with new values
     * @return
     */
    @PutMapping("/users/{email}")
    public ResponseEntity<String> updateUser(@PathVariable("email") String email, @RequestBody User user) {
        boolean response = userService.updateUser(email, user);
        if (response) {
            return new ResponseEntity<>("User was updated successfully.", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Cannot find user with email=" + email, HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Assigns a new password to a user
     * @param credentials
     * @return
     */
    @PutMapping("/users")
    public ResponseEntity<String> updatePassword(@RequestBody Credentials credentials) {
        try {
            boolean response = userService.updatePassword(credentials);
            if (response) {
                return new ResponseEntity<>("User was updated successfully.", HttpStatus.OK);
            } else {
                return new ResponseEntity<>("Cannot find user with email=" + credentials.getUserEmail(), HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>("Cannot update password.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Deletes user from the database
     * This will also delete any correlations they had to other tables in the database
     * @param email
     * @return
     */
    @DeleteMapping("/users/{email}")
    public ResponseEntity<String> deleteUser(@PathVariable("email") String email) {
        try {
            int result = userService.deleteUser(email);
            if (result == 0) {
                return new ResponseEntity<>("Cannot find user with email=" + email, HttpStatus.OK);
            }
            return new ResponseEntity<>("User was deleted successfully.", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("Cannot delete user.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Deletes all users in the database
     * This will also delete any correlations to other table, meaning that in practice all tables beside COURSES will be wiped clean
     * @return
     */
    @DeleteMapping("/users")
    public ResponseEntity<String> deleteAllUsers() {
        try {
            int numRows = userService.deleteAllUsers();
            return new ResponseEntity<>("Deleted " + numRows + " user(s) successfully.", HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>("Cannot delete users.", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Returns a list of all users with admin privileges
     * @return
     */
    @GetMapping("/users/admins")
    public ResponseEntity<List<User>> findAdmins() {
        try {
            List<User> admins = userService.findAdmins();
            if (admins.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(admins, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Returns a list of all users registered as students
     * @return
     */
    @GetMapping("/users/students")
    public ResponseEntity<List<User>> findStudents() {
        try {
            List<User> admins = userService.findStudents();
            if (admins.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(admins, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
