package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model;

import java.util.List;

public class Course {

    private long courseId;
    private String courseCode;
    private String courseName;
    private int totObligs;
    private int requiredObligs;
    private String courseInformation;
    private String campus;
    private String building;
    private String room;
    private int numOfTables;
    private String year;
    private boolean courseActive;
    private List<String> assistants;
    private boolean activeQueue;

    public Course() {
    }

    public Course(long courseId, String courseCode, String courseName, int totObligs, int requiredObligs,
                  String courseInformation, String campus, String building, String room, int numOfTables,
                  String year, boolean courseActive, List<String> assistants, boolean activeQueue) {
        this.courseId = courseId;
        this.courseCode = courseCode;
        this.courseName = courseName;
        this.totObligs = totObligs;
        this.requiredObligs = requiredObligs;
        this.courseInformation = courseInformation;
        this.campus = campus;
        this.building = building;
        this.room = room;
        this.numOfTables = numOfTables;
        this.year = year;
        this.courseActive = courseActive;
        this.assistants = assistants;
        this.activeQueue = activeQueue;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public int getNumOfTables() {
        return numOfTables;
    }

    public void setNumOfTables(int numOfTables) {
        this.numOfTables = numOfTables;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public boolean getCourseActive() {
        return courseActive;
    }

    public void setCourseActive(boolean courseActive) {
        this.courseActive = courseActive;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public int getTotObligs() {
        return totObligs;
    }

    public void setTotObligs(int totObligs) {
        this.totObligs = totObligs;
    }

    public int getRequiredObligs() {
        return requiredObligs;
    }

    public void setRequiredObligs(int requiredObligs) {
        this.requiredObligs = requiredObligs;
    }

    public String getCourseInformation() {
        return courseInformation;
    }

    public void setCourseInformation(String courseInformation) {
        this.courseInformation = courseInformation;
    }

    public List<String> getAssistants() {
        return assistants;
    }

    public boolean getActiveQueue() {
        return activeQueue;
    }

    public void setActiveQueue(boolean activeQueue) {
        this.activeQueue = activeQueue;
    }

    public void setAssistants(List<String> assistants) {
        this.assistants = assistants;
    }
}
