package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model;

public class SeveralUsers {

    private String userString;

    public SeveralUsers() {
    }

    public SeveralUsers(String userString) {
        this.userString = userString;
    }

    public String getUserString() {
        return userString;
    }

    public void setUserString(String userString) {
        this.userString = userString;
    }
}
