package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model;

public class Credentials {
    private String userEmail;
    private String password;

    public Credentials() {
    }

    public Credentials(String userEmail, String password) {
        this.userEmail = userEmail;
        this.password = password;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
