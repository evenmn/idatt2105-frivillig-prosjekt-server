package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model;

import java.util.List;

public class StudentCourse {

    private String userEmail;
    private long courseId;
    private List<Integer> obligs;

    public StudentCourse() {
    }

    public StudentCourse(String userEmail, long courseId) {
        this.userEmail = userEmail;
        this.courseId = courseId;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public List<Integer> getObligs() {
        return obligs;
    }

    public void setObligs(List<Integer> obligs) {
        this.obligs = obligs;
    }
}
