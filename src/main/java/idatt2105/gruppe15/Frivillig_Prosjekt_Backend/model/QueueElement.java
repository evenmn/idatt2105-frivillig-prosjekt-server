package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model;

import java.util.Date;
import java.util.List;

public class QueueElement {

    private long queueId;
    private List<String> userEmail;
    private long courseId;
    private String obligNr;
    private List<Integer> obligList;
    private Date date;
    private String campus;
    private String building;
    private String room;
    private int seat;
    private String message;
    private boolean validation;
    private String assistant;

    public QueueElement() {
    }

    public QueueElement(long queueId, List<String> userEmail, long courseId, String obligNr,
                        List<Integer> obligList, Date date, String campus, String building,
                        String room, int seat, String message, boolean validation, String assistant) {
        this.queueId = queueId;
        this.userEmail = userEmail;
        this.courseId = courseId;
        this.obligNr = obligNr;
        this.obligList = obligList;
        this.date = date;
        this.campus = campus;
        this.building = building;
        this.room = room;
        this.seat = seat;
        this.message = message;
        this.validation = validation;
        this.assistant = assistant;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public boolean getValidation() {
        return validation;
    }

    public void setValidation(boolean validation) {
        this.validation = validation;
    }

    public long getQueueId() {
        return queueId;
    }

    public void setQueueId(long queueId) {
        this.queueId = queueId;
    }

    public List<String> getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(List<String> userEmail) {
        this.userEmail = userEmail;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public String getObligNr() {
        return obligNr;
    }

    public List<Integer> getObligList() {
        return obligList;
    }

    public void setObligList(List<Integer> obligList) {
        this.obligList = obligList;
    }

    public void setObligNr(String obligNr) {
        this.obligNr = obligNr;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public int getSeat() {
        return seat;
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAssistant() {
        return assistant;
    }

    public void setAssistant(String assistant) {
        this.assistant = assistant;
    }
}
