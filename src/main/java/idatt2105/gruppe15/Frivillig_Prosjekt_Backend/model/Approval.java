package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model;

public class Approval {

    private String userEmail;
    private long courseId;
    private int obligNr;
    private String feedback;

    public Approval() {
    }

    public Approval(String userEmail, long courseId, int obligNr, String feedback) {
        this.userEmail = userEmail;
        this.courseId = courseId;
        this.obligNr = obligNr;
        this.feedback = feedback;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public int getObligNr() {
        return obligNr;
    }

    public void setObligNr(int obligNr) {
        this.obligNr = obligNr;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
}
