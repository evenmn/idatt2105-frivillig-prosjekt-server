//package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.security;
//
//import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.User;
//import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.service.UserService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.stereotype.Component;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Component
//public class CustomAuthenticationProvider implements AuthenticationProvider {
//
//    @Autowired
//    UserService userService;
//
//    @Override
//    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
//        final String email = authentication.getName();
//        final String password = authentication.getCredentials().toString();
//
//        User user = userService.getUserByEmail(email);
//
//        if (user==null) {
//            return null;
//        }
//
//        if(user.getUserEmail().equals(email) && user.getPassword().equals(password)) {
//            String role;
//            if (user.getAdmin()) {
//                role = "ADMIN";
//            } else {
//                role = "USER";
//            }
//            final List<GrantedAuthority> grantedAuths = new ArrayList<>();
//            grantedAuths.add(new SimpleGrantedAuthority(role));
//            final UserDetails principal = new org.springframework.security.core.userdetails.User(email, password, grantedAuths);
//            return new UsernamePasswordAuthenticationToken(principal, password, grantedAuths);
//        }
//        return null;
//    }
//
//    @Override
//    public boolean supports(Class<?> authentication) {
//        return authentication.equals(UsernamePasswordAuthenticationToken.class);
//    }
//}
