package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.repository;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.Credentials;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcUserRepository implements UserRepository{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int save(User user, String password) {
        return jdbcTemplate.update("INSERT INTO users (userEmail, firstName, lastName, password, admin) VALUES (?,?,?,?,?)",
                user.getUserEmail(), user.getFirstName(), user.getLastName(), password, user.getAdmin());
    }

    @Override
    public int update(User user) {
        return jdbcTemplate.update("UPDATE users SET firstName=?, lastName=?, admin=? WHERE userEmail=?",
                user.getFirstName(), user.getLastName(), user.getAdmin(), user.getUserEmail());
    }

    @Override
    public int updatePassword(Credentials credentials) {
        return jdbcTemplate.update("UPDATE users SET password=? WHERE userEmail=?",
                credentials.getPassword(), credentials.getUserEmail());
    }

    @Override
    public User findByEmail(String email) {
        try {
            User user = jdbcTemplate.queryForObject("SELECT * FROM users WHERE userEmail=?",
                    BeanPropertyRowMapper.newInstance(User.class), email);
            return user;
        } catch (DataAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Credentials findCredentialsByEmail(String email) {
        try {
            Credentials credentials = jdbcTemplate.queryForObject("SELECT * FROM users WHERE userEmail=?",
                    BeanPropertyRowMapper.newInstance(Credentials.class), email);
            return credentials;
        } catch (DataAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int deleteByEmail(String email) {
        jdbcTemplate.update("DELETE FROM students_queues WHERE userEmail=?", email);
        jdbcTemplate.update("DELETE FROM assistants WHERE userEmail=?", email);
        jdbcTemplate.update("DELETE FROM approval WHERE userEmail=?", email);
        jdbcTemplate.update("DELETE FROM users_courses WHERE userEmail=?", email);
        return jdbcTemplate.update("DELETE FROM users WHERE userEmail=?", email);
    }

    @Override
    public List<User> findAll() {
        return jdbcTemplate.query("SELECT * FROM users", BeanPropertyRowMapper.newInstance(User.class));
    }

    @Override
    public List<User> findAllAssistants() {
        return jdbcTemplate.query("SELECT * FROM users WHERE userEmail IN (SELECT users.userEmail FROM users JOIN assistants ON (users.userEmail = assistants.userEmail))",
                BeanPropertyRowMapper.newInstance(User.class));
    }

    @Override
    public List<User> findStudentsByCourseId(long id) {
        return jdbcTemplate.query("SELECT * FROM users WHERE userEmail IN (SELECT users.userEmail FROM users JOIN users_courses ON (users.userEmail = users_courses.userEmail)" +
                " WHERE users_courses.courseId = ?)", BeanPropertyRowMapper.newInstance(User.class), id);
    }

    @Override
    public List<User> findAssistantsByCourseId(long id) {
        return jdbcTemplate.query("SELECT * FROM users WHERE userEmail IN (SELECT users.userEmail FROM users JOIN assistants ON (users.userEmail = assistants.userEmail)" +
                " WHERE assistants.courseId = ?)", BeanPropertyRowMapper.newInstance(User.class), id);
    }

    @Override
    public List<User> findAdmins() {
        return jdbcTemplate.query("SELECT * FROM users WHERE admin = 1", BeanPropertyRowMapper.newInstance(User.class));
    }

    @Override
    public List<User> findStudents() {
        return jdbcTemplate.query("SELECT * FROM users WHERE admin = 0", BeanPropertyRowMapper.newInstance(User.class));
    }

    @Override
    public boolean validatePassword(Credentials credentials) {
        String password = jdbcTemplate.queryForObject("SELECT password FROM users WHERE userEmail=?", String.class, credentials.getUserEmail());
        return credentials.getPassword().equals(password);
    }

    @Override
    public int deleteAll() {
        jdbcTemplate.update("DELETE FROM students_queues");
        jdbcTemplate.update("DELETE FROM assistants");
        jdbcTemplate.update("DELETE FROM approval");
        jdbcTemplate.update("DELETE FROM users_courses");
        return jdbcTemplate.update("DELETE FROM users");
    }
}
