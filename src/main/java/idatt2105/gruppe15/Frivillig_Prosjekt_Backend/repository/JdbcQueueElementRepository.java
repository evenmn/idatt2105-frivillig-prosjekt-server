package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.repository;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.QueueElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcQueueElementRepository implements QueueElementRepository{

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public int save(QueueElement qe) {
        return jdbcTemplate.update("INSERT INTO queueElements (obligNr, date, campus, building, room, seat, validation, message, courseId) VALUES (?,?,?,?,?,?,?,?,?)",
                qe.getObligNr(), qe.getDate(), qe.getCampus(), qe.getBuilding(), qe.getRoom(), qe.getSeat(), qe.getValidation(), qe.getMessage(), qe.getCourseId());
    }

    @Override
    public int assignStudent(long queueId, String email) {
        return jdbcTemplate.update("INSERT INTO students_queues (userEmail, queueId) VALUES (?,?)", email, queueId);
    }

    @Override
    public int update(QueueElement qe) {
        return jdbcTemplate.update("UPDATE queueElements SET obligNr=?, date=?, campus=?, building=?, room=?, seat=?, validation=?, message=?, courseId=? WHERE queueId=?",
                qe.getObligNr(), qe.getDate(), qe.getCampus(), qe.getBuilding(), qe.getRoom(), qe.getSeat(), qe.getValidation(), qe.getMessage(), qe.getCourseId(), qe.getQueueId());
    }

    @Override
    public QueueElement findById(long queueId) {
        try {
            QueueElement qe = jdbcTemplate.queryForObject("SELECT * FROM queueElements WHERE queueId=?",
                    BeanPropertyRowMapper.newInstance(QueueElement.class), queueId);
            return qe;
        } catch (DataAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public QueueElement findWithoutId(QueueElement qe) {
        try {
            QueueElement dbQe = jdbcTemplate.queryForObject("SELECT * FROM queueElements " +
                            "WHERE obligNr=? AND date=? AND building=? AND room=? AND seat=? AND message=? AND courseId=?",
                    BeanPropertyRowMapper.newInstance(QueueElement.class),
                    qe.getObligNr(), qe.getDate(), qe.getBuilding(), qe.getRoom(), qe.getSeat(), qe.getMessage(), qe.getCourseId());
            return dbQe;
        } catch (DataAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int deleteById(long queueId) {
        jdbcTemplate.update("DELETE FROM students_queues WHERE queueId=?", queueId);
        return jdbcTemplate.update("DELETE FROM queueElements WHERE queueId=?", queueId);
    }

    @Override
    public List<QueueElement> findAll() {
        return jdbcTemplate.query("SELECT * FROM queueElements", BeanPropertyRowMapper.newInstance(QueueElement.class));
    }

    @Override
    public List<QueueElement> findByCourseId(long courseId) {
        return jdbcTemplate.query("SELECT * FROM queueElements WHERE courseId = ?",
                BeanPropertyRowMapper.newInstance(QueueElement.class), courseId);
    }

    @Override
    public List<QueueElement> findByStudentEmail(String userEmail) {
        return jdbcTemplate.query("SELECT * FROM queueElements WHERE queueId IN (SELECT queueId FROM students_queues WHERE userEmail=?)",
                BeanPropertyRowMapper.newInstance(QueueElement.class), userEmail);
    }

    @Override
    public List<QueueElement> findByCourseIdAndEmail(long courseId, String userEmail) {
        return jdbcTemplate.query("SELECT * FROM queueElements WHERE queueId IN (SELECT queueId FROM students_queues WHERE userEmail=?) AND courseId=?",
                BeanPropertyRowMapper.newInstance(QueueElement.class), userEmail, courseId);
    }

    @Override
    public List<String> findStudents(long queueId) {
        return jdbcTemplate.queryForList("SELECT userEmail FROM students_queues WHERE queueId=?",
                String.class, queueId);
    }

    @Override
    public int assignAssistant(long id, String assistant) {
        return jdbcTemplate.update("UPDATE queueElements SET assistant=? WHERE queueId=?", assistant, id);
    }

    @Override
    public int removeAssistant(long id) {
        return jdbcTemplate.update("UPDATE queueElements SET assistant=NULL WHERE queueId=?", id);
    }

    @Override
    public int deleteAll() {
        jdbcTemplate.update("DELETE FROM students_queues");
        return jdbcTemplate.update("DELETE FROM queueElements");
    }

    @Override
    public int clearQueue(long courseId) {
        jdbcTemplate.update("DELETE FROM students_queues WHERE queueId IN (SELECT queueId FROM queueElements WHERE courseId=?)", courseId);
        return jdbcTemplate.update("DELETE FROM queueElements WHERE courseId=?", courseId);
    }
}
