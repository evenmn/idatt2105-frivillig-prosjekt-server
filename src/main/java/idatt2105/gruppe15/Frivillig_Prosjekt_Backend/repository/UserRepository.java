package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.repository;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.Credentials;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.User;

import java.util.List;

public interface UserRepository {

    int save(User user, String Password);
    int update(User user);
    int updatePassword(Credentials credentials);
    User findByEmail(String email);
    Credentials findCredentialsByEmail(String email);
    int deleteByEmail(String email);
    List<User> findAll();
    List<User> findAllAssistants();
    List<User> findStudentsByCourseId(long id);
    List<User> findAssistantsByCourseId(long id);
    List<User> findAdmins();
    List<User> findStudents();
    boolean validatePassword(Credentials credentials);
    int deleteAll();
}
