package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.repository;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.Approval;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.StudentCourse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcStudentCourseRepository implements StudentCourseRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public int save(StudentCourse sc) {
        return jdbcTemplate.update("INSERT INTO users_courses (userEmail, courseId) VALUES (?,?)",
                sc.getUserEmail(), sc.getCourseId());
    }

    @Override
    public int update(StudentCourse sc) {
        return 0;
    }

    @Override
    public StudentCourse findByStudentCourse(String userEmail, long courseId) {
        try {
            StudentCourse sc = jdbcTemplate.queryForObject("SELECT * FROM users_courses WHERE userEmail=? AND courseId=?",
                    BeanPropertyRowMapper.newInstance(StudentCourse.class), userEmail, courseId);
            return sc;
        } catch (DataAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int deleteByStudentCourse(StudentCourse sc) {
        jdbcTemplate.update("DELETE FROM approval WHERE userEmail=? AND courseId=?");
        return jdbcTemplate.update("DELETE FROM users_courses WHERE userEmail=? AND courseId=?",
                sc.getUserEmail(), sc.getCourseId());
    }

    @Override
    public List<StudentCourse> findAll() {
        return jdbcTemplate.query("SELECT * FROM users_courses", BeanPropertyRowMapper.newInstance(StudentCourse.class));
    }

    @Override
    public List<Integer> findObligs(StudentCourse sc) {
        return jdbcTemplate.queryForList("SELECT obligNr FROM approval WHERE userEmail=? AND courseId=?",
                Integer.class, sc.getUserEmail(), sc.getCourseId());
    }

    @Override
    public int approveOblig(Approval approval) {
        return jdbcTemplate.update("INSERT INTO approval (userEmail, courseId, obligNr, feedback) VALUES (?,?,?,?)",
                approval.getUserEmail(), approval.getCourseId(), approval.getObligNr(), approval.getFeedback());
    }

    @Override
    public int deleteAll() {
        jdbcTemplate.update("DELETE FROM approval");
        return jdbcTemplate.update("DELETE FROM users_courses");
    }

    @Override
    public int deleteAllApprovals() {
        return jdbcTemplate.update("DELETE FROM approval");
    }
}
