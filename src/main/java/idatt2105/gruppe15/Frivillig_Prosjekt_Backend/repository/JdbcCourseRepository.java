package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.repository;

import com.fasterxml.jackson.databind.deser.impl.BeanPropertyMap;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.Course;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.QueueElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JdbcCourseRepository implements CourseRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public int save(Course course) {
        return jdbcTemplate.update("INSERT INTO courses (courseCode, courseName, totObligs, requiredObligs, courseInformation, " +
                        "campus, building, room, numOfTables, year, courseActive, activeQueue) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",
                course.getCourseCode(), course.getCourseName(), course.getTotObligs(), course.getRequiredObligs(), course.getCourseInformation(),
                course.getCampus(), course.getBuilding(), course.getRoom(), course.getNumOfTables(), course.getYear(),course.getCourseActive(), course.getActiveQueue());
    }

    @Override
    public int update(Course course) {
        return jdbcTemplate.update("UPDATE courses SET courseCode=?, courseName=?, totObligs=?, requiredObligs=?, courseInformation=?, " +
                        "campus=?, building=?, room=?, numOfTables=?, year=?, courseActive=?, activeQueue=? WHERE courseId=?",
                course.getCourseCode(), course.getCourseName(), course.getTotObligs(), course.getRequiredObligs(), course.getCourseInformation(),
                course.getCampus(), course.getBuilding(), course.getRoom(), course.getNumOfTables(), course.getYear(), course.getCourseActive(),
                course.getActiveQueue(), course.getCourseId());
    }

    @Override
    public Course findById(long courseId) {
        try {
            Course course = jdbcTemplate.queryForObject("SELECT * FROM courses WHERE courseId=?",
                    BeanPropertyRowMapper.newInstance(Course.class), courseId);
            return course;
        } catch (DataAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Course findWithoutId(Course course) {
        try {
            Course dbCourse = jdbcTemplate.queryForObject("SELECT * FROM courses WHERE courseCode=? AND year=?",
                    BeanPropertyRowMapper.newInstance(Course.class), course.getCourseCode(), course.getYear());
            return dbCourse;
        } catch (DataAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int deleteById(long courseId) {
        jdbcTemplate.update("DELETE FROM assistants WHERE courseId=?", courseId);
        jdbcTemplate.update("DELETE FROM users_courses WHERE courseId=?", courseId);
        return jdbcTemplate.update("DELETE FROM courses WHERE courseId=?", courseId);
    }

    @Override
    public List<Course> findAll() {
        return jdbcTemplate.query("SELECT * FROM courses", BeanPropertyRowMapper.newInstance(Course.class));
    }

    @Override
    public List<Course> findByStudentEmail(String email) {
        return jdbcTemplate.query("SELECT * FROM courses JOIN users_courses ON (courses.courseId = users_courses.courseId) " +
                        "WHERE users_courses.userEmail = ?", BeanPropertyRowMapper.newInstance(Course.class), email);
    }

    @Override
    public List<Course> findByAssistantEmail(String email) {
        return jdbcTemplate.query("SELECT * FROM courses JOIN assistants ON (courses.courseId = assistants.courseId) " +
                "WHERE assistants.userEmail = ?", BeanPropertyRowMapper.newInstance(Course.class), email);
    }

    @Override
    public List<String> findAssistantEmails(long courseId) {
        return jdbcTemplate.queryForList("SELECT userEmail FROM assistants WHERE courseId=?",
                String.class, courseId);
    }

    @Override
    public int getQueueSize(long courseId) {
        return jdbcTemplate.queryForObject("SELECT COUNT(*) FROM queueElements WHERE courseId=?", Integer.class, courseId);
    }

    @Override
    public int addAssistant(long courseId, String userEmail) {
        return jdbcTemplate.update("INSERT INTO assistants (userEmail, courseId) VALUES (?,?)", userEmail, courseId);
    }

    @Override
    public int removeAssistant(long courseId, String userEmail) {
        return jdbcTemplate.update("DELETE FROM assistants WHERE userEmail=? AND courseId=?", userEmail, courseId);
    }

    @Override
    public int removeAllAssistants(long courseId) {
        return jdbcTemplate.update("DELETE FROM assistants WHERE courseId=?", courseId);
    }

    @Override
    public int deleteAll() {
        jdbcTemplate.update("DELETE FROM assistants");
        jdbcTemplate.update("DELETE FROM users_courses");
        return jdbcTemplate.update("DELETE FROM courses");
    }
}
