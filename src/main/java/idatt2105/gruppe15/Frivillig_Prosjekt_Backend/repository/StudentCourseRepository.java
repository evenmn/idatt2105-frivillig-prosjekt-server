package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.repository;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.Approval;
import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.StudentCourse;

import java.util.List;

public interface StudentCourseRepository {

    int save(StudentCourse sc);
    int update(StudentCourse sc);
    StudentCourse findByStudentCourse(String userEmail, long courseId);
    int deleteByStudentCourse(StudentCourse sc);
    List<StudentCourse> findAll();
    List<Integer> findObligs(StudentCourse sc);
    int approveOblig(Approval approval);
    int deleteAll();
    int deleteAllApprovals();
}
