package idatt2105.gruppe15.Frivillig_Prosjekt_Backend.repository;

import idatt2105.gruppe15.Frivillig_Prosjekt_Backend.model.QueueElement;

import java.util.List;

public interface QueueElementRepository {

    int save(QueueElement qe);
    int assignStudent(long queueId, String email);
    int update(QueueElement qe);
    QueueElement findById(long queueId);
    QueueElement findWithoutId(QueueElement qe);
    int deleteById(long queueId);
    List<QueueElement> findAll();
    List<QueueElement> findByCourseId(long courseId);
    List<QueueElement> findByStudentEmail(String userEmail);
    List<QueueElement> findByCourseIdAndEmail(long courseId, String userEmail);
    List<String> findStudents(long queueId);
    int assignAssistant(long id, String assistant);
    int removeAssistant(long id);
    int deleteAll();
    int clearQueue(long courseId);
}
